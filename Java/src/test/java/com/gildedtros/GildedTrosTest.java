package com.gildedtros;

import com.gildedtros.model.Item;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class GildedTrosTest {

    @Test
    void testItemName() {
        var givenItems = new Item[]{new Item("foo", 0, 0)};
        var actualGildedTros = new GildedTros(givenItems);
        assertThat(actualGildedTros.getItems()[0].getName())
                .isEqualTo("foo");
    }

    static Stream<Arguments> getItems() {
        return Stream.of(
                Arguments.of(new Item("Ring of Cleansening Code", 10, 20), 9, 19),
                Arguments.of(new Item("Ring of Cleansening Code", 10, 0), 9, 0),
                Arguments.of(new Item("Ring of Cleansening Code", 0, 10), -1, 8),
                Arguments.of(new Item("Good Wine", 2, 0), 1, 1),
                Arguments.of(new Item("Good Wine", 2, 50), 1, 50),
                Arguments.of(new Item("Elixir of the SOLID", 5, 7), 4, 6),
                Arguments.of(new Item("B-DAWG Keychain", 0, 80), 0, 80),
                Arguments.of(new Item("B-DAWG Keychain", -1, 80), -1, 80),
                Arguments.of(new Item("B-DAWG Keychain", -1, 70), -1, 80),
                Arguments.of(new Item("Backstage passes for Re:Factor", 15, 20), 14, 21),
                Arguments.of(new Item("Backstage passes for Re:Factor", 10, 40), 9, 42),
                Arguments.of(new Item("Backstage passes for Re:Factor", 10, 49), 9, 50),
                Arguments.of(new Item("Backstage passes for HAXX", 5, 40), 4, 43),
                Arguments.of(new Item("Backstage passes for HAXX", 5, 49), 4, 50),
                Arguments.of(new Item("Duplicate Code", 3, 0), 2, 0),
                Arguments.of(new Item("Duplicate Code", 3, 6), 2, 4),
                Arguments.of(new Item("Duplicate Code", 3, 1), 2, 0),
                Arguments.of(new Item("Long Methods", 3, 6), 2, 4),
                Arguments.of(new Item("Ugly Variable Names", 3, 6), 2, 4),
                Arguments.of(new Item("Ugly Variable Names", 3, 2), 2, 0),
                Arguments.of(new Item("Ugly Variable Names", 3, 1), 2, 0),
                Arguments.of(new Item("Ugly Variable Names", 0, 3), -1, 0),
                Arguments.of(new Item("Ugly Variable Names", 0, 2), -1, 0),
                Arguments.of(new Item("Ugly Variable Names", 0, 1), -1, 0),
                Arguments.of(new Item("Ugly Variable Names", 0, 10), -1, 6)
        );
    }

    @ParameterizedTest(name = "{displayName} - [{index}] {arguments}")
    @MethodSource("getItems")
    void testItemsUpdateQuality(Item actualItem, int expectedDaysRemaining, int expectedQuality) {
        //given
        var actualGildedTros = new GildedTros(new Item[]{actualItem});
        //when
        actualGildedTros.updateQuality();
        //then
        assertThat(actualGildedTros.getItems()[0])
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .hasFieldOrPropertyWithValue("daysRemaining", expectedDaysRemaining)
                .hasFieldOrPropertyWithValue("quality", expectedQuality);
    }

}
