package com.gildedtros.strategy;

import com.gildedtros.model.Item;

/**
 * @author Olivier Cappelle
 * @version x.x.x
 * @see
 * @since x.x.x 05/03/2023
 **/
public class LegendaryItemUpdateQualityStrategy implements UpdateQualityStrategy {
    @Override
    public void updateQuality(Item item) {
        item.setQuality(80);
    }
}
