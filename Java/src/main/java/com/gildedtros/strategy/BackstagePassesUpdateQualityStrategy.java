package com.gildedtros.strategy;

import com.gildedtros.model.Item;

/**
 * @author Olivier Cappelle
 * @version x.x.x
 * @see
 * @since x.x.x 05/03/2023
 **/
public class BackstagePassesUpdateQualityStrategy implements UpdateQualityStrategy {
    @Override
    public void updateQuality(Item item) {
        decreaseDaysRemaining(item);
        upgradeQualityByOne(item);

        if (daysRemainingIsLessThenZero(item)) {
            item.setQuality(0);
        } else if (item.getDaysRemaining() <= 5) {
            upgradeQualityByOne(item);
            upgradeQualityByOne(item);
        } else if (item.getDaysRemaining() <= 10) {
            upgradeQualityByOne(item);
        }
    }
}
