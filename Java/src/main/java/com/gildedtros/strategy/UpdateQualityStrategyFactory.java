package com.gildedtros.strategy;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * @author Olivier Cappelle
 * @version x.x.x
 * @see
 * @since x.x.x 05/03/2023
 **/
@Getter
@RequiredArgsConstructor
public enum UpdateQualityStrategyFactory {
    GOOD_WINE("Good Wine", new GoodWineUpdateQualityStrategy()),
    B_DAWG_KEYCHAIN("B-DAWG Keychain", new LegendaryItemUpdateQualityStrategy()),
    BACKSTAGE_PASSES_FOR_HAXX("Backstage passes for HAXX", new BackstagePassesUpdateQualityStrategy()),
    BACKSTAGE_PASSES_FOR_REFACTOR("Backstage passes for Re:Factor", new BackstagePassesUpdateQualityStrategy()),
    DUPLICATE_CODE("Duplicate Code", new SmellyItemUpdateQualityStrategy()),
    LONG_METHODS("Long Methods", new SmellyItemUpdateQualityStrategy()),
    UGLY_VARIABLE_NAMES("Ugly Variable Names", new SmellyItemUpdateQualityStrategy());

    private static final UpdateQualityStrategy DEFAULT_STRATEGY = new DefaultUpdateQualityStrategy();
    private final String itemName;
    private final UpdateQualityStrategy updateQualityStrategy;

    public static UpdateQualityStrategy getUpdateQualityStrategy(String itemName) {
        return Arrays.stream(values())
                .filter(elem -> elem.itemName.equalsIgnoreCase(itemName))
                .findFirst()
                .map(elem -> elem.updateQualityStrategy)
                .orElse(DEFAULT_STRATEGY);
    }
}
