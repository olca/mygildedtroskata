package com.gildedtros.strategy;

import com.gildedtros.model.Item;

/**
 * @author Olivier Cappelle
 * @version x.x.x
 * @see
 * @since x.x.x 05/03/2023
 **/
@FunctionalInterface
public interface UpdateQualityStrategy {
    void updateQuality(Item item);

    default void decreaseDaysRemaining(Item item) {
        item.setDaysRemaining(item.getDaysRemaining() - 1);
    }

    default void upgradeQualityByOne(Item item) {
        if (item.getQuality() < 50) {
            item.setQuality(item.getQuality() + 1);
        }
    }

    default void degradeQualityByOne(Item item) {
        if (item.getQuality() > 0) {
            item.setQuality(item.getQuality() - 1);
        }
    }

    default boolean daysRemainingIsLessThenZero(Item item) {
        return item.getDaysRemaining() < 0;
    }
}
