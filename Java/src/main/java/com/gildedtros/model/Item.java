package com.gildedtros.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Item {
    private String name;
    private int daysRemaining;
    private int quality;

    public Item(String name, int daysRemaining, int quality) {
        this.name = name;
        this.daysRemaining = daysRemaining;
        this.quality = quality;
    }

    @Override
    public String toString() {
        return this.name + ", " + this.daysRemaining + ", " + this.quality;
    }
}
