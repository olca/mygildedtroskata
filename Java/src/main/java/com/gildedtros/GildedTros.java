package com.gildedtros;

import com.gildedtros.model.Item;
import lombok.Getter;

import java.util.Arrays;

import static com.gildedtros.strategy.UpdateQualityStrategyFactory.getUpdateQualityStrategy;

@Getter
class GildedTros {
    private final Item[] items;

    public GildedTros(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        Arrays.stream(items)
                .forEach(item -> getUpdateQualityStrategy(item.getName()).updateQuality(item));
    }
}